<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<title>Battlefield Analysis</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
</style>
</head>
<body><div id="main">
    <h1>Battlefield Analysis</h1>
    <style>
        h1{
            text-align: center;
        }
    </style>
    <h2>Latest Critiques</h2>
    <?php>
        $stmt = $mysqli->prepare("SELECT critique FROM reports ORDER BY posted DESC limit 10=?");
       if(!$stmt){
             printf("Query Prep Failed: %s\n", $mysqli->error);
             exit;
       }

       $stmt->execute();
       $stmt->bind_result($critiques);
       $stmt->store_result();
       echo "<ul>";
       while($stmt->fetch()) {
            echo "<li>" . $critiques . "</li>";
        }
        echo "</ul>";
    ?>
    
    <h2>Battle Statistics</h2>
    <?php>
        $stmt = $mysqli->prepare("SELECT avg(ammunition) FROM reports GROUP BY soldiers ORDER BY soldiers DESC =?");
       if(!$stmt){
             printf("Query Prep Failed: %s\n", $mysqli->error);
             exit;
       }

        $stmt->execute();
        $stmt->bind_result($average_ammunition);
        $stmt->store_result();
        echo "<table>";
        echo "<th>Number of Soliders</th>";
        echo "<th>Pounds of Ammunition per Second</th>";
        while($stmt->fetch()) {
            echo "<td>" . $average_ammunition . "</td>";
        }
        echo "</ul>";
        
        echo "<a href="http:file://Users/BerkeleyUser/Documents/CSE%20330S/Spring2015-ICA-Corey-Salzer-429163/battlefield-submit.html>Submit a New Battle Report/a>";
        
    ?>
</div></body>
</html>