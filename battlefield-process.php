<?php
   session_start();
   if(isset($_POST['ammo']) || isset($_POST['soldiers']) || isset($_POST['duration']) || isset($_POST['critique'])) {
    echo "Please make sure that all input fields are set before submitting";
        session_unset();
        session_destroy();
   }
   
   $ammo= intval($_POST['ammo']);
   $soldiers= intval($_POST['soldiers']);
   $duration= floatval($_POST['duration']);
   $critique= $_POST['critique'];

   
    $mysqli = new mysqli('localhost', 'wustl_inst', 'wustl_pass', 'battlefield');
    
    if($mysqli->connect_errno) {
           printf("Connection Failed: %s\n", $mysqli->connect_error);
           exit;
    }
    
    $stmtid = $mysqli->prepare("INSERT INTO reports (ammunition, soldiers, duration, critique) VALUE (?, ?, ?, ?)");
    if(!$stmtid){
       printf("Query Prep Failed: %s\n", $mysqli->error);
       exit;
    }
    $stmtid->bind_param('iids', $ammo, $soldiers, $duration, $critique);
    $stmtid->execute();
    $stmtid->close();
    header("Location: battlefield-submit.html");
   
   ?>